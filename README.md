# Hatch Game Engine Example Projects

This repository contains example projects for the Hatch Game Engine. The branch you're in, `main`, is not an example project; please navigate through the other branches in order to find said example projects.
